//
// Created by herman on 1/17/20.
//

#ifndef SDL_PLOTTER_COLOR_H
#define SDL_PLOTTER_COLOR_H

#endif //SDL_PLOTTER_COLOR_H

struct Color {
    int red, green, blue, alpha;

    Color(int r, int g, int b, int a = 0) : red(r), green(g), blue(b), alpha(a) {}
};