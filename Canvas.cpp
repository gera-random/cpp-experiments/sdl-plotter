//
// Created by herman on 1/17/20.
//

#include "Canvas.h"

Canvas::Canvas(int width, int height) : width(width), height(height) {
    SDL_CreateWindowAndRenderer(width, height, 0, &window, &renderer);
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
    SDL_RenderClear(renderer);

    update();
}

Canvas::~Canvas() {
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
}

void Canvas::update() {
    SDL_RenderPresent(renderer);
    SDL_RenderClear(renderer);
}

void Canvas::drawPoint(int x, int y, Color c) {
    SDL_SetRenderDrawColor(renderer, c.red, c.green, c.blue, c.alpha);
    SDL_RenderDrawPoint(renderer, x, y);
}

int Canvas::getWidth() const {
    return width;
}

int Canvas::getHeight() const {
    return height;
}
