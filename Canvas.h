//
// Created by herman on 1/17/20.
//

#ifndef SDL_PLOTTER_CANVAS_H
#define SDL_PLOTTER_CANVAS_H

#include <SDL.h>

#include "Color.h"

class Canvas {
public:
    Canvas(int width, int height);

    ~Canvas();

    void update();

    void drawPoint(int x, int y, Color c);

    int getWidth() const;

    int getHeight() const;

private:
    int width, height;

    SDL_Renderer *renderer;
    SDL_Window *window;
};


#endif //SDL_PLOTTER_CANVAS_H
