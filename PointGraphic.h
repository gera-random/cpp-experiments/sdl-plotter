//
// Created by herman on 1/17/20.
//

#ifndef SDL_PLOTTER_POINTGRAPHIC_H
#define SDL_PLOTTER_POINTGRAPHIC_H

#include "AbstractPointGraphic.h"
#include "Canvas.h"

class PointGraphic : public AbstractPointGraphic {
public:
    PointGraphic(Canvas &canvas, int x0, int y0, Color c, double scale = 1.0);

    void draw(double x, double y) override;

private:
    Canvas &canvas;
    int x0, y0;
    Color c;
    double scale;
};


#endif //SDL_PLOTTER_POINTGRAPHIC_H
