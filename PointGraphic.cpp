//
// Created by herman on 1/17/20.
//

#include "PointGraphic.h"

void PointGraphic::draw(double x, double y) {
    canvas.drawPoint(x0 + int(x * scale), y0 + int(y * scale), c);
}

PointGraphic::PointGraphic(Canvas &canvas, int x0, int y0, Color c, double scale) : canvas(canvas), x0(x0), y0(y0),
                                                                                    c(c), scale(scale) {

}
