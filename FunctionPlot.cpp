//
// Created by herman on 1/17/20.
//

#include "FunctionPlot.h"

#include <utility>

FunctionPlot::FunctionPlot(std::function<double(double)> f, double a, double b, double delta) : f(std::move(f)), a(a),
                                                                                                b(b), delta(delta) {

}

double FunctionPlot::getY(double x) {
    return f(x);
}

void FunctionPlot::draw(AbstractPointGraphic &g) {
    double x = a;
    while (x < b) {
        g.draw(x, f(x));
        x += delta;
    }
}
