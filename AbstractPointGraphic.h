//
// Created by herman on 1/17/20.
//

#ifndef SDL_PLOTTER_ABSTRACTPOINTGRAPHIC_H
#define SDL_PLOTTER_ABSTRACTPOINTGRAPHIC_H


class AbstractPointGraphic {
public:
    virtual void draw(double, double) = 0;
};


#endif //SDL_PLOTTER_ABSTRACTPOINTGRAPHIC_H
