//
// Created by herman on 1/17/20.
//

#ifndef SDL_PLOTTER_FUNCTIONPLOT_H
#define SDL_PLOTTER_FUNCTIONPLOT_H

#include <functional>

#include "AbstractPointGraphic.h"

class FunctionPlot {
public:
    FunctionPlot(std::function<double(double)> f, double a, double b, double delta);

    double getY(double x);

    void draw(AbstractPointGraphic &);

private:
    std::function<double(double)> f;
    double a, b, delta;
};


#endif //SDL_PLOTTER_FUNCTIONPLOT_H
