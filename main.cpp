#include <SDL.h>
#include <complex>

#include "Canvas.h"
#include "FunctionPlot.h"
#include "PointGraphic.h"

int main(void) {
    SDL_Event event;

    const int windowWidth = 640;
    const int windowHeight = 480;

    SDL_Init(SDL_INIT_VIDEO);
    Canvas plot(windowWidth, windowHeight);

    FunctionPlot fp1([](auto x) { return x * sin(log10(x)); }, 0, 100, 0.001);
    PointGraphic pg1(plot, 100, 100, Color(200, 255, 200));
    fp1.draw(pg1);

    FunctionPlot fp2([](auto x) { return sin(x); }, 0, tan(1) * 4, 0.001);
    PointGraphic pg2(plot, 100, 100, Color(255, 200, 200), 20.0);
    fp2.draw(pg2);

    FunctionPlot fp3([](auto x) { return (x * x); }, -5, 5, 0.001);
    PointGraphic pg3(plot, 100, 100, Color(200, 200, 255), 4.0);
    fp3.draw(pg3);

    plot.update();

    while (true) {
        if (SDL_PollEvent(&event) && event.type == SDL_QUIT)
            break;
    }

    SDL_Quit();
    return EXIT_SUCCESS;
}